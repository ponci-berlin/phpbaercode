<?php

declare(strict_types=1);

namespace ponci_berlin\phpbaercode;

use ponci_berlin\phpbaercode\cose\Encrypt0Message;
use ponci_berlin\phpbaercode\cose\Signature;
use ponci_berlin\phpbaercode\cose\SignMessage;
use DateTime;
use DateTimeInterface;

use phpseclib3\Crypt\EC;
use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;

class BaerCode
{
    const VERSION = 1;

    private string $first_name;
    private string $last_name;
    private DateTime $date_of_birth;
    private array $procedures;
    private string $operator;
    private string $kid;
    private bool $result;
    private EC\PrivateKey $ecdsa_private_key;
    private string $aesKey;
    private string $baercode_b64;

    public function __construct(
        string $first_name,
        string $last_name,
        DateTime $date_of_birth,
        array $procedures,
        string $operator,
        bool $result,
        string $kid,
        EC\PrivateKey $ecdsa_private_key,
        string $aesKey
    )
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->date_of_birth = $date_of_birth;
        $this->procedures = $procedures;
        $this->operator = $operator;
        $this->result = $result;
        $this->kid = $kid;
        $this->ecdsa_private_key = $ecdsa_private_key;
        $this->aesKey = $aesKey;
        $this->baercode_b64 = "";
    }

    public function generate(): string
    {
        $credential = new Credential(
            $this->first_name,
            $this->last_name,
            $this->date_of_birth,
            $this->procedures,
            $this->operator,
            $this->result,
        );
        $credential_cbor = $credential->encode_cbor();

        $enc0msg = new Encrypt0Message($credential_cbor, $this->kid);
        $enc0msg->encrypt_aesgcm($this->aesKey);
        $enc0msg_cbor = $enc0msg->encode_cbor();

        
        $signmsg = new SignMessage($enc0msg_cbor);
        $sig = new Signature($signmsg, $this->ecdsa_private_key, $this->kid);
        $signmsg->sign();

        $baercode = $this->add_version($signmsg->encode_cbor());

        $this->baercode_b64 = base64_encode($baercode);
        return $this->baercode_b64;
    }

    public function generate_qr(): string
    {
        if ($this->baercode_b64 == "") {
            $this->generate();
        }
        $qroptions = new QROptions([
            'outputType' => QRCode::OUTPUT_IMAGE_PNG,
            'eccLevel' => QRCode::ECC_L,
            'imageBase64' => false,
            'imageTransparent' => false,
            'dataModeOverride' => 'Byte',
        ]);
        $qrcode = new QRCode($qroptions);
        return $qrcode->render($this->baercode_b64);
    }

    private function add_version(string $baercode_cbor)
    {
        $version_bin = pack('v', $this::VERSION);
        return $version_bin . $baercode_cbor;
    }
}
