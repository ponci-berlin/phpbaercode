<?php
declare(strict_types=1);

namespace ponci_berlin\phpbaercode\cose;

use CBOR\ByteStringObject;
use CBOR\ListObject;
use CBOR\TextStringObject;

use ErrorException;

/*
 * Encrypt0Message represents a COSE Encrypt0 message, defined at:
 * https://tools.ietf.org/html/rfc8152#section-5.2
 */
class Encrypt0Message
{
    const TAG = 16;
    const CONTEXT = "Encrypt0";
    const IV_SIZE = 12;

    private array $aes_gcm_names = array(
        128 => "A128GCM",
        192 => "A192GCM",
        256 => "A256GCM",
    );

    private string $unencrypted_payload;
    private string $ciphertext;
    private COSEHeaders $headers;

    /*
     * The constructor creates a barebones message with the unencrypted payload.
     */
    public function __construct(string $payload, string $kid)
    {
        $this->unencrypted_payload = $payload;
        $this->headers = new COSEHeaders();
        $this->ciphertext = '';
        $this->headers->set_kid($kid);
    }


    /*
     * encrypt_aesgcm encrypts the payload.
     */
    public function encrypt_aesgcm(string $key) 
    {
        $bitlength = mb_strlen($key, '8bit')*8;
        if (! array_key_exists($bitlength, $this->aes_gcm_names)) {
            throw new ErrorException("Not supported keylength: $bitlength");
        }

        $alg = $this->aes_gcm_names[$bitlength];
        $this->headers->set_alg($alg);
        $iv = openssl_random_pseudo_bytes($this::IV_SIZE);
        $additional_data = $this->build_additional_data();

        $tag = null;
        $ciphertext = openssl_encrypt($this->unencrypted_payload, 'aes-128-gcm', $key, OPENSSL_RAW_DATA, $iv, $tag, $additional_data, 16);
        $this->ciphertext = implode([$ciphertext, $tag]);
        $this->headers->set_iv($iv);
    }

    private function build_additional_data(): string
    {
        $enc_struct = new ListObject([
            new TextStringObject($this::CONTEXT),
            new ByteStringObject($this->headers->encode_protected()),
            new ByteStringObject("")
        ]);
        return (string)$enc_struct;
    }

    /*
     * to_cbor encodes the message to CBOR, and returns the byte string.
     */
    public function encode_cbor(): string
    {
        if (strlen($this->ciphertext) == 0) {
            throw new ErrorException("Not serialising unencrypted message.");
        }
        $message = Encrypt0Tag::create(new ListObject([
            new ByteStringObject($this->headers->encode_protected()),
            $this->headers->encode_unprotected(),
            new ByteStringObject($this->ciphertext),
        ]));
        return (string) $message;
    }


}

?>
