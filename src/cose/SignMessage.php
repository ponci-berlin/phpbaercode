<?php
declare(strict_types=1);

namespace ponci_berlin\phpbaercode\cose;

use CBOR\ByteStringObject;
use CBOR\ListObject;

/**
 * SignMessage represents a COSE SignMessage, defined at:
 * https://tools.ietf.org/html/rfc8152#section-4.1
 */
class SignMessage
{
    public array $signatures;
    public COSEHeaders $headers;
    public string $payload;

    /**
     * The constructor creates a message without any signers.
     */
    public function __construct(string $payload)
    {
        $this->signatures = array();
        $this->headers = new COSEHeaders();
        $this->payload = $payload;
    }

    /**
     * sign signs with all the signers.
     */
    public function sign()
    {
        foreach ($this->signatures as $signature)
        {
            $signature->sign();
        }
    }

    /**
     * encode_cbor encodes the SignMessage to CBOR.
     */
    public function encode_cbor(): string
    {
        $signature_array = new ListObject();
        foreach ($this->signatures as $signature) {
            $signature_array->add($signature->encode_cbor());
        }
        $message = SignTag::create(new ListObject([
            new ByteStringObject($this->headers->encode_protected()),
            $this->headers->encode_unprotected(),
            new ByteStringObject($this->payload),
            $signature_array
        ]));
        return (string) $message;
    }

}

?>
