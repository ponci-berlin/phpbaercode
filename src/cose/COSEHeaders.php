<?php

declare(strict_types=1);

namespace ponci_berlin\phpbaercode\cose;

use CBOR\ByteStringObject;
use CBOR\MapObject;
use CBOR\SignedIntegerObject;
use CBOR\UnsignedIntegerObject;
use ErrorException;

/**
 * COSEHeaders is an abstraction for the COSE headers defined at:
 * https://tools.ietf.org/html/rfc8152#section-3
 */
class COSEHeaders
{
    private array $protected;
    private array $unprotected;

    /**
     * the constructor only initialises the arrays.
     */
    public function __construct() {
        $this->protected = array();
        $this->unprotected = array();
    }


    /**
     * set_kid sets the KID in the unprotected headers
     */
    public function set_kid(string $kid)
    {
        $this->unprotected[4] = $kid;
    }

    /**
     * set_iv sets the IV in unprotected headers.
     */
    public function set_iv(string $iv)
    {
        $this->unprotected[5] = $iv;
    }

    /**
     * set_alg sets the algorithm in the protected headers.
     * Note that only ES512, A128GCM, A192GCM, and A256GCM are supported for now.
     */
    public function set_alg(string $alg) 
    {
        switch ($alg) {
            case "ES512":
                $this->protected[1] = -36;
                break;
            case "A128GCM":
                $this->protected[1] = 1;
                break;
            case "A192GCM":
                $this->protected[1] = 2;
                break;
            case "A256GCM":
                $this->protected[1] = 3;
                break;
            default:
                throw new ErrorException("Not a supported algorithm: $alg");
                break;
        }
    }

    /**
     * encode_protected encodes the protected headers.
     * e.g. it creates a CBOR map and returns the serialised string.
     */
    public function encode_protected(): string
    {
        if (count($this->protected) == 0) {
            return '';
        }
        $header_object = new MapObject();
        foreach ($this->protected as $key => $value) {
            if ($value >= 0) {
                $header_object->add(UnSignedIntegerObject::create($key), UnsignedIntegerObject::create($value));
            } else {
                $header_object->add(UnSignedIntegerObject::create($key), SignedIntegerObject::create($value));
            }
        }
        return (string)$header_object;
    }

    /**
     * encode_unprotected encodes the unprotected headers.
     * e.g. it creates a CBOR map and returns it unserialised.
     */
    public function encode_unprotected(): MapObject
    {
        $header_object = new MapObject();
        foreach ($this->unprotected as $key => $value) {
            switch ($key) {
                case 4;
                case 5;
                    $header_object->add(UnsignedIntegerObject::create($key), new ByteStringObject($value));
                break;
                default;
                    if ($value >= 0) {
                        $header_object->add(UnSignedIntegerObject::create($key), UnsignedIntegerObject::create($value));
                    } else {
                        $header_object->add(UnSignedIntegerObject::create($key), SignedIntegerObject::create($value));
                    }
                break;
            }
        }
        return $header_object;
    }
}



