<?php

declare(strict_types=1);

namespace ponci_berlin\phpbaercode;

use CBOR\ByteStringObject;
use CBOR\ListObject;
use CBOR\OtherObject\FalseObject;
use CBOR\OtherObject\TrueObject;
use CBOR\SignedIntegerObject;
use CBOR\TextStringObject;
use CBOR\UnsignedIntegerObject;
use DateTime;

class Credential
{
    public string $first_name;
    public string $last_name;
    public DateTime $date_of_birth;
    public array $procedures;
    public string $operator;
    public bool $result;

    public function __construct(
        string $first_name,
        string $last_name,
        DateTime $date_of_birth,
        array $procedures,
        string $operator,
        bool $result
    )
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->date_of_birth = $date_of_birth;
        $this->procedures = $procedures;
        $this->operator = $operator;
        $this->result = $result;
    }

    public function encode_cbor(): string
    {
        $procedures = new ListObject();
        foreach ($this->procedures as $procedure) {
            $procedures->add($procedure->encode_unserialsed_cbor());
        }
        $bool_obj = new FalseObject();
        if ($this->result) {
            $bool_obj = new TrueObject();
        }
        $integerClassName = ( $this->date_of_birth->getTimestamp() >= 0 ? UnsignedIntegerObject::class : SignedIntegerObject::class );
        $cbor_encoded = new ListObject([
            new TextStringObject($this->first_name),
            new TextStringObject($this->last_name),
            $integerClassName::create($this->date_of_birth->getTimestamp()),
            // TODO: Disease type hardcoded for now
            UnsignedIntegerObject::create(1),
            $procedures,
            new TextStringObject($this->operator),
            $bool_obj,
        ]);
        return (string)$cbor_encoded;
    }
}

?>
